package com.example.bisschopdominic.parkingxl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bisschopdominic.parkingxl.R;
import com.example.bisschopdominic.parkingxl.models.ParkingLots;

import java.util.List;

public class CustumParkingAdapter extends ArrayAdapter<ParkingLots> {

    public CustumParkingAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public CustumParkingAdapter(Context context, int resource, List<ParkingLots> parkinglots) {
        super(context, resource, parkinglots);
    }

    public CustumParkingAdapter(Context context, List<ParkingLots> parkinglots) {
        super(context, -1, parkinglots);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row_layout, parent, false);

        TextView title = (TextView) v.findViewById(R.id.Parkingstitle);

        ParkingLots p = getItem(position);
        title.setText(p.getTitle());


        return v;
    }
}
