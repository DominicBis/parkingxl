package com.example.bisschopdominic.parkingxl;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.view.Menu;
import android.view.MenuItem;

import com.example.bisschopdominic.parkingxl.R;

public class LandingPageActivity extends AppCompatActivity implements DetailListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landingpage);
    }

    @SuppressLint("RestrictedApi")
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_landingpage, menu);

//        if(menu instanceof MenuBuilder){
//            MenuBuilder m = (MenuBuilder) menu;
//            //noinspection RestrictedApi
//            m.setOptionalIconsVisible(true);
//        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_profile:
                startActivity(new Intent(this, ProfilePageActivity.class));
                return true;
            case R.id.action_logout:
                startActivity(new Intent(this, LoginPageActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!this.getClass().getSimpleName().equals("LandingPageActivity")) {
            super.onBackPressed();
        }
    }

    public void setId(int id) {
        if (findViewById(R.id.fragment_container2) != null) {
            DetailFragment detailFragment =  (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container2);
            detailFragment.setId(id);
        } else {
            Intent intent = new Intent(this,DetailPageActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        }
    }
}
