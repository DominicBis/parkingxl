package com.example.bisschopdominic.parkingxl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bisschopdominic.parkingxl.R;
import com.example.bisschopdominic.parkingxl.models.ParkingLots;

import java.util.List;

public class CustomDetailParkingAdapter extends ArrayAdapter<ParkingLots> {

    public CustomDetailParkingAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public CustomDetailParkingAdapter(Context context, int resource, List<ParkingLots> parkinglots) {
        super(context, resource, parkinglots);
    }

    public CustomDetailParkingAdapter(Context context, List<ParkingLots> parkinglots) {
        super(context, -1, parkinglots);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.detail_row_layout, parent, false);

        TextView title = v.findViewById(R.id.parkingsTitle);
        TextView adress = v.findViewById(R.id.address);
        TextView freePlaces = v.findViewById(R.id.freePlaces);

        ParkingLots p = getItem(position);
        title.setText(p.getTitle());
        adress.setText(p.getAddress());
        freePlaces.setText(p.getPlaces() - p.getPlacesNow());


        return v;
    }
}
