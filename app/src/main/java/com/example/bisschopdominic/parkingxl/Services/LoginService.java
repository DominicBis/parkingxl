package com.example.bisschopdominic.parkingxl.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.bisschopdominic.parkingxl.R;

import org.json.JSONObject;


public class LoginService extends Service {

    final String ip = "http://192.168.1.107";
    final String url = ip + ":3000";
    public static final String TAG = "User";
    private StringRequest stringRequest; // Assume this exists.
    private RequestQueue mRequestQueue;  // Assume this exists.
    private MySingleton volley;
    private Context act;
    private APICallBackListener apiCallBack;
    private APICallBackListenerObject apiCallbackObject;

    LoginService() {
    }

    public APICallBackListener getApiCallBack() {
        return apiCallBack;
    }

    public APICallBackListenerObject getApiCallbackObject() {
        return apiCallbackObject;
    }

    public void setApiCallBack(APICallBackListener apiCallBack) {
        this.apiCallBack = apiCallBack;
    }

    public void setApiCallBackObject(APICallBackListenerObject apiCallbackObject) {
        this.apiCallbackObject = apiCallbackObject;
    }


    public RequestQueue getmRequestQueue() {
        return mRequestQueue;
    }

    public LoginService(Context activity) {
        act = activity;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void getUserByData() {

        RequestQueue queue = MySingleton.getInstance(act.getApplicationContext()).getRequestQueue();
        JsonObjectRequest jsArrRequest = new JsonObjectRequest
                (Request.Method.GET, url + "/User", null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Succees", response.toString());
                        if (apiCallbackObject != null) {
                            apiCallbackObject.onCallBackRecieved(response);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR", error.toString());
                    }
                });

        queue.add(jsArrRequest);
    }


}