package com.example.bisschopdominic.parkingxl.Services;

import org.json.JSONObject;

public interface APICallBackListenerObject {
    void onCallBackRecieved(JSONObject jsonObject);
}
