package com.example.bisschopdominic.parkingxl.Services;

import org.json.JSONArray;

public interface APICallBackListener {
    void onCallBackRecieved(JSONArray jsonArray);
}
