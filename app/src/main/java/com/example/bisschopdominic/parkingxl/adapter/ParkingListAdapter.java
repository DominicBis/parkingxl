package com.example.bisschopdominic.parkingxl.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bisschopdominic.parkingxl.DetailPageActivity;
import com.example.bisschopdominic.parkingxl.R;
import com.example.bisschopdominic.parkingxl.models.ParkingLots;

import java.util.List;

public class ParkingListAdapter extends RecyclerView.Adapter<ParkingListAdapter.ViewHolder> {
    private List<ParkingLots> mDataSet;

    public ParkingListAdapter(List<ParkingLots> dataSet){
        mDataSet = dataSet;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context mContext = view.getContext();
                    Log.d("ParkingListAdapter", "Element " + getAdapterPosition() + " clicked.");
                    Intent intent = new Intent(mContext, DetailPageActivity.class);
                    intent.putExtra("id", mDataSet.get(getAdapterPosition()).getId());
                    mContext.startActivity(intent);
                }
            });
            textView = (TextView) view.findViewById(R.id.Parkingstitle);
        }

        public TextView getTextView() {
            return textView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int parkingType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.listparkings_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.getTextView().setText(mDataSet.get(position).getTitle());
//        holder.getTextView().setTextColor(Color.BLUE);
    }

    @Override
    public int getItemCount() {
        if (mDataSet == null){
            return 0;
        }
        return mDataSet.size();
    }
}
