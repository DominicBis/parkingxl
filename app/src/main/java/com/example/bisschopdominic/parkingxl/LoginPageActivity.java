package com.example.bisschopdominic.parkingxl;

import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.bisschopdominic.parkingxl.Services.APICallBackListenerObject;
import com.example.bisschopdominic.parkingxl.Services.LoginService;
import com.example.bisschopdominic.parkingxl.models.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONObject;
import java.lang.reflect.Type;

public class LoginPageActivity extends AppCompatActivity {

    private LoginService loginService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginpage);

        Button btn = (Button)findViewById(R.id.btnLogin);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginService = new LoginService(getApplicationContext());

                final User user = new User();
                final TextView textViewUsername = (TextView)findViewById(R.id.txtUsername);
                final TextView textViewPassword = (TextView)findViewById(R.id.txtPassword);

                loginService.getUserByData();

                loginService.setApiCallBackObject(new APICallBackListenerObject() {
                    @Override
                    public void onCallBackRecieved(JSONObject jsonObject) {
                        final Gson gson = new Gson();
                        final Type type = new TypeToken<User>(){}.getType();
                        final User user= gson.fromJson(jsonObject.toString(), type);

                        String usernameText = textViewUsername.getText().toString();
                        String passwordText = textViewPassword.getText().toString();

                        Log.d("User", user.toString());
                        Log.d("username", textViewUsername.getText().toString());
                        Log.d("password", textViewPassword.getText().toString());

                        if (usernameText.equals("")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginPageActivity.this);
                            builder.setCancelable(false);
                            builder.setTitle("Gebruikersnaam vereist");
                            builder.setMessage("U moet een gebruikersnaam invullen." + "\n" + "Probeer het opnieuw!");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialoginterface, int i) {
                                }
                            });
                            builder.show();
                        } else if (passwordText.equals("")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginPageActivity.this);
                            builder.setCancelable(false);
                            builder.setTitle("Wachtwoord vereist");
                            builder.setMessage("U moet een wachtwoord invullen." + "\n" + "Probeer het opnieuw!");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialoginterface, int i) {
                                }
                            });
                            builder.show();
                        } else {
                            if (user.username.equals(usernameText) && user.password.equals(passwordText)) {
                                startActivity(new Intent(LoginPageActivity.this, LandingPageActivity.class));
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginPageActivity.this);
                                builder.setCancelable(false);
                                builder.setTitle("Foute combinatie");
                                builder.setMessage("De combinatie die u heeft ingevuld klopt niet." + "\n" + "Probeer het opnieuw!");

                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialoginterface, int i) {
                                    }
                                });
                                builder.show();
                            }
                        }
                    }
                });
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
