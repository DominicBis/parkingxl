package com.example.bisschopdominic.parkingxl;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.bisschopdominic.parkingxl.Services.APICallBackListener;
import com.example.bisschopdominic.parkingxl.Services.ParkingService;
import com.example.bisschopdominic.parkingxl.adapter.CustumParkingAdapter;
import com.example.bisschopdominic.parkingxl.adapter.ParkingListAdapter;
import com.example.bisschopdominic.parkingxl.models.ParkingLots;
import com.example.bisschopdominic.parkingxl.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class ListFragment extends Fragment {
    private List<ParkingLots> parkingList = new ArrayList<ParkingLots>();
    private ParkingService parkingService;
    Context mContext;

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        parkingService = new ParkingService(getActivity());
        final View view = inflater.inflate(R.layout.fragment_list, container, false);

        RecyclerView mRecyclerView = view.findViewById(R.id.listParkings);
        final ParkingListAdapter mAdapter = new ParkingListAdapter(parkingList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        parkingService.getAllParkingLots();

        parkingService.setApiCallBack(new APICallBackListener() {
            @Override
            public void onCallBackRecieved(JSONArray jsonArray) {

                final Gson gson = new Gson();
                final Type type = new TypeToken<List<ParkingLots>>(){}.getType();
                final List<ParkingLots> parkinglots = gson.fromJson(jsonArray.toString(), type);
                Log.d(parkinglots.toString(), "parkingLots");
                for (ParkingLots parking : parkinglots){
                    parkingList.add(parking);
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        return view;
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        parkserv = new ParkingService(getActivity());
//
//        View view = inflater.inflate(R.layout.fragment_list, container, false);
//
//        ListView listView = view.findViewById(R.id.listParkings);
//
//        arrayAdapter = new CustumParkingAdapter(getActivity(), R.layout.row_layout, parkingList);
//        listView.setAdapter(arrayAdapter);
//
//
//        parkserv.getAllParkingLots();
//
//
//        parkserv.setApiCallBack(new APICallBackListener() {
//            @Override
//            public void onCallBackRecieved(JSONArray jsonArray) {
//
//                final Gson gson = new Gson();
//                final Type type = new TypeToken<List<ParkingLots>>(){}.getType();
//                final List<ParkingLots> parkinglots = gson.fromJson(jsonArray.toString(), type);
//                Log.d(parkinglots.toString(), "parkingLots");
//                for (ParkingLots parking : parkinglots){
//                    parkingList.add(parking);
//
//                }
//                arrayAdapter.notifyDataSetChanged();
//            }
//        });
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                int iid = (int) id;
//                DetailListener detailListener = (DetailListener) getActivity();
//                detailListener.setId(iid);
//            }
//        });
//        return view;
//    }
}
