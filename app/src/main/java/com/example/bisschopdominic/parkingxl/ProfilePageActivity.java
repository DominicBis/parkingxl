package com.example.bisschopdominic.parkingxl;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.bisschopdominic.parkingxl.R;

public class ProfilePageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profilepage);

        getProfileData();

        Button btnOpslaan = (Button)findViewById(R.id.btnOpslaan);
        btnOpslaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                storeProfileData();

                AlertDialog.Builder builder = new AlertDialog.Builder(ProfilePageActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Opgeslagen");
                builder.setMessage("Uw profielgegevens zijn opgeslagen!");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public void getProfileData() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("appData", Context.MODE_PRIVATE);

        TextView textViewLastName = (TextView)findViewById(R.id.txtAchternaam);
        TextView textViewFirstName = (TextView)findViewById(R.id.txtVoornaam);
        TextView textViewNumberPlate = (TextView)findViewById(R.id.txtKenteken);

        if (sharedPreferences.contains("firstName")) {
            textViewLastName.setText(sharedPreferences.getString("lastName", "0"));
        }
        if (sharedPreferences.contains("lastName")) {
            textViewFirstName.setText(sharedPreferences.getString("firstName", "0"));
        }
        if (sharedPreferences.contains("numberPlate")) {
            textViewNumberPlate.setText(sharedPreferences.getString("numberPlate", "0"));
        }
    }

    public void storeProfileData() {
        TextView textViewLastName2 = (TextView)findViewById(R.id.txtAchternaam);
        TextView textViewFirstName2 = (TextView)findViewById(R.id.txtVoornaam);
        TextView textViewNumberPlate2 = (TextView)findViewById(R.id.txtKenteken);

        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferences( "appData", Context.MODE_PRIVATE ).edit();
        sharedPreferencesEditor.putString( "lastName", textViewLastName2.getText().toString() );
        sharedPreferencesEditor.putString( "firstName", textViewFirstName2.getText().toString() );
        sharedPreferencesEditor.putString( "numberPlate", textViewNumberPlate2.getText().toString() );
        sharedPreferencesEditor.apply();
    }
}


