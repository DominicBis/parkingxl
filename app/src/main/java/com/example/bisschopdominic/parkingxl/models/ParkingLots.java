package com.example.bisschopdominic.parkingxl.models;

public class ParkingLots {
    public int id;
    public String title;
    public int places;
    public int placesNow;
    public String address;
    public double latitude;
    public double longitude;
    public boolean favorite;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ParkingLots(String title, int places, int placesNow, String address) {
        this.title = title;
        this.places = places;
        this.placesNow = placesNow;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public int getPlacesNow() {
        return placesNow;
    }

    public void setPlacesNow(int placesNow) {
        this.placesNow = placesNow;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
