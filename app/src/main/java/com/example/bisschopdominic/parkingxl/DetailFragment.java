package com.example.bisschopdominic.parkingxl;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bisschopdominic.parkingxl.Services.APICallBackListenerObject;
import com.example.bisschopdominic.parkingxl.Services.ParkingService;
import com.example.bisschopdominic.parkingxl.adapter.CustomDetailParkingAdapter;
import com.example.bisschopdominic.parkingxl.models.ParkingLots;
import com.example.bisschopdominic.parkingxl.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;

public class DetailFragment extends Fragment implements OnMapReadyCallback {


    private GoogleMap googleMap;

    private String titleText, addressText;
    private int freePlacesText;
    private TextView title, freeplaces, address;
    private ParkingService parkserv;
    private CustomDetailParkingAdapter customDetailParkingAdapter;

    private LatLng Parking;
    private GoogleMap map;
    private LatLng latLng;
    private int totalSpaces;

    public LatLng getParking() {
        return Parking;
    }

    public void setParking(LatLng parking) {
        Parking = parking;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_detail, container, false);


        title = fragmentView.findViewById(R.id.parkingsTitle);
        freeplaces = fragmentView.findViewById(R.id.freePlaces);
        address = fragmentView.findViewById(R.id.address);

        return fragmentView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager childFragmentManager = getChildFragmentManager();
        FragmentTransaction tx = childFragmentManager.beginTransaction();

        SupportMapFragment fragment = new SupportMapFragment();
        fragment.getMapAsync(this);
        tx.add(R.id.fragment_map, fragment);

        tx.commit();

    }

    public void setId(int id) {
        parkserv = new ParkingService(getActivity());

        parkserv.getParkingLotsById(id);

        parkserv.setApiCallBackObject(new APICallBackListenerObject() {

            @Override
            public void onCallBackRecieved(JSONObject jsonObject) {

                final Gson gson = new Gson();
                final Type type = new TypeToken<ParkingLots>() {
                }.getType();
                final ParkingLots parkinglots = gson.fromJson(jsonObject.toString(), type);


                titleText = parkinglots.getTitle();
                freePlacesText = parkinglots.getPlacesNow();
                addressText = parkinglots.getAddress();
                totalSpaces = parkinglots.getPlaces();




                title.setText(titleText);
                freeplaces.setText(freePlacesText + "");
                address.setText(addressText);


                if ((totalSpaces * 75 / 100) <= freePlacesText) {
                    freeplaces.setTextColor(Color.RED);
                } else if ((totalSpaces * 50 / 100) <= freePlacesText) {
                    freeplaces.setTextColor(Color.MAGENTA);
                } else if ((totalSpaces * 25/ 100) <= freePlacesText) {
                    freeplaces.setTextColor(Color.BLUE);
                }

                latLng = new LatLng(parkinglots.getLatitude(), parkinglots.getLongitude());

                if (map != null) {
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                }
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (latLng != null) {
            map.addMarker(new MarkerOptions().position(latLng).title(title.getText().toString()));
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,13));
        }
    }
}