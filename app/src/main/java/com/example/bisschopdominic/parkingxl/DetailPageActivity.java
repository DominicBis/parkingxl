package com.example.bisschopdominic.parkingxl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.example.bisschopdominic.parkingxl.Services.ParkingService;
import com.example.bisschopdominic.parkingxl.adapter.CustomDetailParkingAdapter;
import com.example.bisschopdominic.parkingxl.models.ParkingLots;
import com.example.bisschopdominic.parkingxl.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;

import java.util.List;

public class DetailPageActivity extends AppCompatActivity implements DetailListener {
    List<ParkingLots> dataSource;
    CustomDetailParkingAdapter adapter;

    private ArrayAdapter<ParkingLots> arrayAdapter;

    MapView mMapView;
    private GoogleMap googleMap;
    private ParkingService parkserv;
    private ParkingLots park;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailpage);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id", -1);
        if( id != -1) {
            setId(id);
        }
    }

    public void setId(int position) {
        DetailFragment detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container2);
        detailFragment.setId(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
