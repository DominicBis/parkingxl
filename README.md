----------------------
Before Start
----------------------

You have to install json-server.
Execute the following command in the AndroidStudio Terminal:

    npm install -g json-server

This command will install a server which you need to get the data from JSON files.

When the server is installed, you will have to run the server.

Before running the next command you will have to put your own IP-address in the ParkingService
There is a String named "ip". Change the IP-address that is there to your own IP-address.

Now execute the following command in the AndroidStudio Terminal to start the server:

    json-server --host <YOUR IP-ADDRESS> db.json

Make sure you change <YOUR IP-ADDRESS> to your own IP-address.
You can find your own IP-address by opening CMD in Windows and execute the command: 

    ipconfig

Now you can run the app and everything will be running smoothly!